/* Select the section with an id of container without using querySelector. */
let containerIdElement = document.getElementById("container");
//console.log(containerIdElement);

/* Select the section with an id of container using querySelector */
let containerQuerySelector = document.querySelector("#container");
//console.log(containerQuerySelector);

/* Select all of the list items with a class of "second". */
let classSecondListItems = document.querySelectorAll("li.second");
//console.log(classSecondListItems);

/* Select a list item with a class of third, but only the list item inside of the ol tag. */

let classThirdOlitem = document.querySelector("ol li.third");
//console.log(classThirdOlitem);

/*Give the section with an id of container the text "Hello!" */

containerIdElement.append("Hello!");
//console.log(containerIdElement);

/*Add the class main to the div with a class of footer. */

let divWithclassFooter = document.querySelector("div.footer");
divWithclassFooter.classList.add("main");

/* Remove the class main on the div with a class of footer. */
divWithclassFooter.classList.remove("main");

/*Create a new li element.*/

let newListItem = document.createElement("li");

/*Give the li the text "four"*/

newListItem.textContent = "four";

/*Append the li to the ul element. */

let ulElement = document.querySelector("ul");
ulElement.appendChild(newListItem);

/* - Loop over all of the lis inside the ol tag and give them a background color of "green". */

let olList = document.querySelectorAll("ol li");

for (let listEle of olList) {
  listEle.style.backgroundColor = "green";
}

/*Remove the div with a class of footer. */

let containerWithFooterClass = document.querySelector("div.footer");
containerWithFooterClass.remove();
